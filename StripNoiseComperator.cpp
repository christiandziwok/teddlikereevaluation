// #define WITH_ERRORS_YES true
// #define WITH_STD_COUTS

#include "CollectStripValues.h"
#include <iostream>
#include <fstream>
#include "TFile.h"
#include <vector>
#include <utility>
#include "TROOT.h"

int main(int argc, char *argv[])
{   
    int argSize = argc;
    if (argSize == 1)
        return 1;
    else if (argSize == 3)
    {
        FILE_FOLDER_BASE_LINE_LVL  = TString(argv[1]);
        FILE_FOLDER_SMALL_DISTANCE = TString(argv[2]);
        // FILE_FOLDER_LARGE_DISTANCE = TString(argv[3]);
        IS_WITH_SMALL_FILE = true;
        // IS_WITH_LARGE_FILE = true;
    }
    else if (argSize == 4)
    {
        FILE_FOLDER_BASE_LINE_LVL  = TString(argv[1]);
        FILE_FOLDER_SMALL_DISTANCE = TString(argv[2]);
        FILE_FOLDER_LARGE_DISTANCE = TString(argv[3]);
        IS_WITH_SMALL_FILE = true;
        IS_WITH_LARGE_FILE = true;
    }
    else if (argSize == 5) 
        if (   !strcmp(argv[1],"--base")
            && !strcmp(argv[1],"--small")
            && !strcmp(argv[1],"--large")
            )
        {
            FILE_FOLDER_BASE_LINE_LVL  = TString(argv[1]);
            FILE_FOLDER_SMALL_DISTANCE = TString(argv[2]);
            FILE_FOLDER_LARGE_DISTANCE = TString(argv[3]);
            IS_WITH_SMALL_FILE = true;
            IS_WITH_LARGE_FILE = true;
            WITH_STD_COUTS = bool(atoi(argv[4]));
        }
        else
            for (int index=1;index<argSize;index+=2)
            {
                if (strcmp(argv[index],"--base"))
                    FILE_FOLDER_BASE_LINE_LVL = TString(argv[index+1]);
                if (strcmp(argv[index],"--small"))
                {
                    FILE_FOLDER_SMALL_DISTANCE = TString(argv[index+1]);
                    IS_WITH_SMALL_FILE = true;
                }
                if (strcmp(argv[index],"--large"))
                {
                    FILE_FOLDER_LARGE_DISTANCE = TString(argv[index+1]);
                    IS_WITH_LARGE_FILE = true;
                }
            }
    else
    {
        if (argSize == 5 || argSize == 7)
        {
            for (int index=1;index<argSize;index+=2)
            {
                if (strcmp(argv[index],"--base"))
                    FILE_FOLDER_BASE_LINE_LVL = TString(argv[index+1]);
                else if (strcmp(argv[index],"--small"))
                {
                    FILE_FOLDER_SMALL_DISTANCE = TString(argv[index+1]);
                    IS_WITH_SMALL_FILE = true;
                }
                else if (strcmp(argv[index],"--large"))
                {
                    FILE_FOLDER_LARGE_DISTANCE = TString(argv[index+1]);
                    IS_WITH_LARGE_FILE = true;
                }

            }
        }
    }

    // TFile *lFile = TFile::Open(FILE_FOLDER+"/CicResults.root");
    TFile *lFileBase  = nullptr;
    TFile *lFileSmall = nullptr;
    TFile *lFileLarge = nullptr;
    candlePlotCbcRhsTop_small = new TH2I();
    candlePlotCbcRhsTop_large = new TH2I();
    candlePlotCbcRhsBottom_small = new TH2I();
    candlePlotCbcRhsBottom_large = new TH2I();
    stripNoiseRhsTop_small = new TGraph();
    stripNoiseRhsTop_large = new TGraph();
    stripNoiseRhsBottom_small = new TGraph();
    stripNoiseRhsBottom_large = new TGraph();

    lFileBase  = TFile::Open(FILE_FOLDER_BASE_LINE_LVL+"/"+PH2ACF_FILE_NAME);
     
    struct loopStrct {
        TFile*first ;
        TString second;
        TH2I* candleRT;
        TH2I* candleRB;
        TGraph* grapgRT;
        TGraph* grapgRB;
    };
    auto listOfFiles = std::vector<loopStrct>{};
    // auto listOfFiles = std::vector<std::pair<TFile*,TString>>{};
    if (IS_WITH_SMALL_FILE){
        lFileSmall = TFile::Open(FILE_FOLDER_SMALL_DISTANCE+"/"+PH2ACF_FILE_NAME);
        auto handle = loopStrct();
        std::tie(handle.first,handle.second,handle.candleRT,handle.candleRB,handle.grapgRT,handle.grapgRB)=
            std::make_tuple(lFileSmall,FILE_FOLDER_SMALL_DISTANCE,candlePlotCbcRhsTop_small,candlePlotCbcRhsBottom_small,stripNoiseRhsTop_small,stripNoiseRhsBottom_small);
        listOfFiles.push_back(handle);
        // listOfFiles.push_back(std::make_pair(lFileSmall,FILE_FOLDER_SMALL_DISTANCE));
    }
    if (IS_WITH_LARGE_FILE){
        lFileLarge = TFile::Open(FILE_FOLDER_LARGE_DISTANCE+"/"+PH2ACF_FILE_NAME);
         auto handle = loopStrct();
        std::tie(handle.first,handle.second,handle.candleRT,handle.candleRB,handle.grapgRT,handle.grapgRB)=
            std::make_tuple(lFileLarge,FILE_FOLDER_LARGE_DISTANCE,candlePlotCbcRhsTop_large,candlePlotCbcRhsBottom_large,stripNoiseRhsTop_large,stripNoiseRhsBottom_large);
       listOfFiles.push_back(handle);
        // listOfFiles.push_back(std::make_pair(lFileLarge,FILE_FOLDER_LARGE_DISTANCE));
    }
    std::ofstream outfile;
    for (auto lVariation : listOfFiles){
        auto lFileVari = lVariation.first;
        auto lPathVari = lVariation.second;        
        std::vector<std::vector<double>> lNoisePerFeh {{0},{0}};
        std::cout << "Compare base to :"<<lPathVari.View()<<std::endl;
        outfile.open(std::string(TString(lPathVari+"/stripNoiseCompBaseLvl.dat").View()).c_str(),std::ofstream::trunc);
        outfile << "#ChnOnFEH;VariLvlMinusBaseLvl;ChnOnChip;ChipOnFEH;FEH"<< std::endl;
        for (int side=0;side<2;side++)
        {

            for (int chip=0;chip<8;chip++)
            {
                std::vector<std::pair<double,double>> noiseVals = MergeTwoListsToListOfPairs(
                    GetListOfNoiseValues(lFileBase, side,chip),
                    GetListOfNoiseValues(lFileVari, side,chip)
                );
                auto diffsVariMinunsBase = DiffListOfPairs(noiseVals);
                if (chip != 0)
                    lNoisePerFeh[side].insert(lNoisePerFeh[side].end(),diffsVariMinunsBase.begin(),diffsVariMinunsBase.end());
                else
                    lNoisePerFeh[side] = diffsVariMinunsBase;
                int cnt{0};
                
                for (const auto &stripValdiffs : diffsVariMinunsBase)
                {
                    if (WITH_STD_COUTS)
                        std::cout << chip*diffsVariMinunsBase.size()+cnt<< "\t"<< stripValdiffs << "\t"<< cnt<<"\t" << chip <<"\t" << side<< std::endl;
                    outfile << chip*diffsVariMinunsBase.size()+cnt<< ";"<< stripValdiffs << ";"<< cnt<<";" << chip <<";" << side<< std::endl;
                    cnt++;
                }
            }
        }
        lFileVari->Close();
        outfile.close();
        TFile *lFileOut = new TFile(lPathVari+"/VariationPlots.root","RECREATE");
        MakeCandlePlots(lNoisePerFeh,-5.,5.);
        lVariation.candleRT->Add(candlePlotCbcRhsTop);
        lVariation.candleRB->Add(candlePlotCbcRhsBottom);
        MakeStripNoiseCanvi(lNoisePerFeh,-5.,5.);
        *lVariation.grapgRT=*stripNoiseRhsTop;
        *lVariation.grapgRB=*stripNoiseRhsBottom;
        lFileOut->Close();
    }
    // lFileBase->Close();
    TFile *lFileCompOut = new TFile(FILE_FOLDER_BASE_LINE_LVL+"/ComparisonPlots.root","RECREATE");
    MakeComparisonCandlePlots(-5.,5.);
    lFileCompOut->Close();
    return 0;
}