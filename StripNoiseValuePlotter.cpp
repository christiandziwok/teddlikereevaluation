// #define WITH_ERRORS_YES true

#include <iostream>
#include <fstream>
#include "TROOT.h"
#include "TFile.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TH1.h"
#include "TH2.h"
#include "TCanvas.h"
#include "TLine.h"

#include <vector>
#include <utility>

#include "CollectStripValues.h"

int main(int argc, char *argv[])
{   
    if (argc == 1) return 1;
    else FILE_FOLDER = TString(argv[1]);
    TFile *lFile = TFile::Open(FILE_FOLDER+"/"+PH2ACF_FILE_NAME);
    std::ofstream outfile;
    outfile.open(std::string(TString(FILE_FOLDER+"/stripNoise.dat").View()).c_str(),std::ofstream::trunc);
    
    #ifdef WITH_ERRORS_YES
        outfile << "#ChnOnFEH;NoiseInVcth;Error;ChnOnChip;ChipOnFEH;FEH"<< std::endl;
        std::vector<<std::vector<std::pair<double,double>>> lNoisePerFeh {{{0,0}},{{0,0}}};
    #else
        outfile << "#ChnOnFEH;NoiseInVcth;ChnOnChip;ChipOnFEH;FEH"<< std::endl;
        std::vector<std::vector<double>> lNoisePerFeh {{0},{0}};
    #endif
    for (int side=0;side<2;side++)
    {
        for (int chip=0;chip<8;chip++)
        {
            #ifdef WITH_ERRORS_YES
                auto noiseVals = GetListOfNoiseAndErrorValues(lFile, side,chip);
            #else
                auto noiseVals = GetListOfNoiseValues(lFile, side,chip);
            #endif

            if (chip != 0)
                lNoisePerFeh[side].insert(lNoisePerFeh[side].end(),noiseVals.begin(),noiseVals.end());
            else
                lNoisePerFeh[side] = noiseVals;
            int cnt{0};
            
            for (auto stripVal : noiseVals)
            {
                #ifdef WITH_ERRORS_YES
                    outfile << chip*noiseVals.size()+cnt<< ";"<< stripVal.first << ";"<< stripVal.second << ";"<< cnt<<";" << chip <<";" << side<< std::endl;
                #else
                    outfile << chip*noiseVals.size()+cnt<< ";"<< stripVal << ";"<< cnt<<";" << chip <<";" << side<< std::endl;
                    std::cout << chip*noiseVals.size()+cnt<< ";"<< stripVal << ";"<< cnt<<";" << chip <<";" << side<< std::endl;
                #endif

                cnt++;
            }
        }
    }
    outfile.close();
    lFile->Close();

    TFile *lFileOut = new TFile(FILE_FOLDER+"/CandlePlots.root","RECREATE");
    MakeCandlePlots(lNoisePerFeh);
    MakeStripNoiseCanvi(lNoisePerFeh);
    lFileOut->Close();
    return 0;
}


