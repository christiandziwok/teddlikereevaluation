#ifndef COLLECT_STRIP_VALUES
#define COLLECT_STRIP_VALUES
#define STRIP_NOISE_RESOLUTION 0.001
#define VCTH_LVL 170.

#define PH2ACF_FILE_NAME "CicResults.root"

#include "string.h"
#include <utility>
#include <map>
#include <vector>
#include "TROOT.h"
#include "TString.h"
#include "TFile.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TH1.h"
#include "TH2.h"
#include "THStack.h"
#include "TCanvas.h"
#include "TLine.h"
#include "TLegend.h"
#include "TGaxis.h"

TString FILE_FOLDER;
TString FILE_FOLDER_BASE_LINE_LVL;
TString FILE_FOLDER_SMALL_DISTANCE;
TString FILE_FOLDER_LARGE_DISTANCE;

TH2I * candlePlotCbcRhsTop_small = nullptr;
TH2I * candlePlotCbcRhsTop_large = nullptr;
TH2I * candlePlotCbcRhsBottom_small = nullptr;
TH2I * candlePlotCbcRhsBottom_large = nullptr;
TGraph * stripNoiseRhsTop_small = nullptr;
TGraph * stripNoiseRhsTop_large = nullptr;
TGraph * stripNoiseRhsBottom_small = nullptr;
TGraph * stripNoiseRhsBottom_large = nullptr;



TH2I * candlePlotCbcRhsTop = nullptr;
TH2I * candlePlotCbcRhsBottom = nullptr;
TGraph * stripNoiseRhsTop = nullptr;
TGraph * stripNoiseRhsBottom = nullptr;
TGraphErrors * stripNoiseRhsTopE = nullptr;
TGraphErrors * stripNoiseRhsBottomE = nullptr;

bool IS_WITH_SMALL_FILE{false};
bool IS_WITH_LARGE_FILE{false};
bool WITH_STD_COUTS{true};
std::map<std::string,int> SIDE2NBR {{"rhs",0},{"lhs",1}};
std::map<int,std::string> NBR2SIDE {{0,"rhs"},{1,"lhs"}};

TString FileBasePath(int pSide, int pChip);
TString StripNoiseHistName(int pSide, int pChip);
TString StripPedestalHistName(int pSide, int pChip);
std::vector<double> GetListOfNoiseValues(TFile *pFile, int pSide, int pChip);
std::vector<std::pair<double,double>> GetListOfNoiseAndErrorValues(TFile *pFile, int pSide, int pChip);

std::vector<std::pair<double,double>> MergeTwoListsToListOfPairs(std::vector<double> pListFirst,std::vector<double> pListSecond);
std::vector<double> DiffListOfPairs(const std::vector<std::pair<double,double>> &pListOfPairs);


void MakeCandlePlots(const std::vector<std::vector<std::pair<double,double>>> &pNoisePerFeh,double pLowBin=0.,double pHighBin=30.);
void MakeCandlePlots(const std::vector<std::vector<double>> &pNoisePerFeh,double pLowBin=0.,double pHighBin=30.);
void MakeStripNoiseCanvi(const std::vector<std::vector<std::pair<double,double>>> &pNoisePerFeh,double pLowY=0.,double pHighY=10.);
void MakeStripNoiseCanvi(const std::vector<std::vector<double>> &pNoisePerFeh,double pLowY=0.,double pHighY=10.);



template <typename T> std::ostream &operator<<(std::ostream &out, const std::vector<T> &v)
{
    out << "{";
    size_t last = v.size() - 1;
    for (size_t i = 0; i < v.size(); ++i)
    {
        out << v[i];
        if (i != last)
            out << ", ";
    }
    out << "}";
    return out;
}
template <typename T, typename U> std::ostream &operator<<(std::ostream &out, const std::vector<std::pair<T,U>> &v)
{
    out << "{";
    size_t last = v.size() - 1;
    for (size_t i = 0; i < v.size(); ++i)
    {
        out << i<<":("<<v[i].first<<","<< v[i].second <<")";
        if (i != last)
            out << ", ";
    }
    out << "}";
    return out;
}

    // "CANDLEX(zhpawMmb)" / "VIOLINX(zhpawMmb)"
    // Where:
    #define b_no_box "0"
    #define b_outer_box "1"
    #define b_filled_box "2"
    #define m_no_median "0"
    #define m_median_as_line "1"
    #define m_median_as_notches "2"
    #define m_median_as_circle "3"
    #define M_no_mean "0"
    #define M_mean_as_dashed_line "1"
    #define M_mean_as_circle "3"
    #define w_no_whisker "0"
    #define w_whisker_to_end "1"
    #define w_whisker_to_max_1pnt5_Times_iqr "2"
    #define a_no_anchor "0"
    #define a_with_anchors "1"
    #define p_no_points "0"
    #define p_only_outliers "1"
    #define p_all "2"
    #define p_all_scattered "3"
    #define h_no_histogram "0"
    #define h_histogram_at_left_bottom "1"
    #define h_histogram_at_right_top "2"
    #define h_histogram_at_both_sides "3"
    #define z_no_zero_indicator "0"
    #define z_zero_indicator "1"

const TString ViolinDrawString = TString::Format("%s%s%s%s%s%s%s%s",
        z_no_zero_indicator,
        h_no_histogram,
        p_only_outliers,
        a_with_anchors,
        w_whisker_to_max_1pnt5_Times_iqr,
        M_mean_as_circle,
        m_median_as_notches,
        b_outer_box
    );
#include "CollectStripValues.cpp"
#endif