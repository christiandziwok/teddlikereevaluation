#ifdef COLLECT_STRIP_VALUES
#include "CollectStripValues.h"
#include <iostream>


TString FileBasePath(int pSide, int pChip){
    return TString::Format("/Detector/Board_0/Module_%d/Chip_%d/",pSide,pChip);
} 
TString StripNoiseHistName(int pSide, int pChip){
    return TString::Format("D_B(0)_M(%d)_StripNoiseDistribution_Chip(%d)",pSide,pChip);
} 
TString StripPedestalHistName(int pSide, int pChip){
    return TString::Format("D_B(0)_M(%d)_StripPedestalDistribution_Chip(%d)",pSide,pChip);
} 

std::vector<double> GetListOfNoiseValues(TFile *pFile, int pSide, int pChip){
    std::vector<double> retVals;
    TH1F *lHist = new TH1F();
    pFile->GetObject(FileBasePath(pSide,pChip)+StripNoiseHistName(pSide,pChip), lHist);
    for (int bin=1; bin < lHist->GetXaxis()->GetNbins()+1;bin++)
        retVals.push_back(lHist->GetBinContent(bin));
    delete lHist;
    return retVals;
}
std::vector<std::pair<double,double>> GetListOfNoiseAndErrorValues(TFile *pFile, int pSide, int pChip){
    std::vector<std::pair<double,double>>  retVals;
    TH1F *lHist = new TH1F();
    pFile->GetObject(FileBasePath(pSide,pChip)+StripNoiseHistName(pSide,pChip), lHist);
    for (int bin=1; bin < lHist->GetXaxis()->GetNbins()+1;bin++)
        retVals.push_back(
            std::make_pair( 
                lHist->GetBinContent(bin),
                lHist->GetBinError(bin)
                )
            );
    delete lHist;
    return retVals;
}

std::vector<std::pair<double,double>> MergeTwoListsToListOfPairs(std::vector<double> pListFirst,std::vector<double> pListSecond){
    std::vector<std::pair<double,double>> retVals;
    for (unsigned int index=0;index<std::max(pListFirst.size(),pListSecond.size());index++){
        if (index<pListFirst.size() &&index<pListSecond.size())
            retVals.push_back(std::make_pair(pListFirst[index],pListSecond[index]));
        else if (index>=pListFirst.size())
            retVals.push_back(std::make_pair(std::nan(""),pListSecond[index]));
        else if (index>=pListSecond.size())
            retVals.push_back(std::make_pair(pListFirst[index],std::nan("")));
        else 
            retVals.push_back(std::make_pair(std::nan(""),std::nan("")));
    }
    return retVals;
}
std::vector<double> DiffListOfPairs(const std::vector<std::pair<double,double>> &pListOfPairs){
    std::vector<double> retVal;
    for (const auto &pair : pListOfPairs)
        retVal.push_back(pair.second-pair.first);
    return retVal;
}


void MakeCandlePlots(const std::vector<std::vector<std::pair<double,double>>> &pNoisePerFeh,double pLowBin,double pHighBin){
    TCanvas * lCanvas = new TCanvas("CandleNoiseRhs_Canvas","Strip noise candle histograms per CBC - RHS");
    candlePlotCbcRhsTop = new TH2I ("candlePlotCbcRhsTop",TString::Format("Noise distributions per CBC - RHS, Top (resolution: %.3FVcth)",STRIP_NOISE_RESOLUTION),8,0,8, int((pHighBin-pLowBin)/STRIP_NOISE_RESOLUTION),pLowBin,pHighBin);
    candlePlotCbcRhsBottom = new TH2I ("candlePlotCbcRhsBottom",TString::Format("Noise distributions per CBC - RHS, Bottom (resolution: %.3FVcth)",STRIP_NOISE_RESOLUTION),8,0,8, int((pHighBin-pLowBin)/STRIP_NOISE_RESOLUTION),pLowBin,pHighBin);
    int cnt{0};
    TLine *lLine = new TLine();
    for (const auto &noiseVal : pNoisePerFeh[0]){
        if (cnt%2 != 0) 
            candlePlotCbcRhsTop->Fill(std::floor(cnt/254.),noiseVal.first);
        else 
            candlePlotCbcRhsBottom->Fill(std::floor(cnt/254.),noiseVal.first);
        cnt++;
    }
    
    THStack *candleStackRhs = new THStack("candleStackRhs","Noise distributions per CBC - RHS Top(blue) and Bottom(red)");
    lCanvas->cd();
    TString::Format("Noise distributions per CBC - RHS, Bottom (resolution: %.3FVcth)",STRIP_NOISE_RESOLUTION);
    candlePlotCbcRhsTop->SetFillColor(kBlue+1);
    candlePlotCbcRhsTop->SetLineColor(kBlue+1);
    candlePlotCbcRhsTop->SetFillStyle(3004);
    candlePlotCbcRhsBottom->SetFillColor(kRed+2);
    candlePlotCbcRhsBottom->SetLineColor(kRed+2);
    candlePlotCbcRhsBottom->SetFillStyle(3005);
    candlePlotCbcRhsTop->GetXaxis()->SetTitle("CBC");    
    candlePlotCbcRhsTop->GetYaxis()->SetTitle("Noise [Vcth]");
    candlePlotCbcRhsBottom->GetXaxis()->SetTitle("CBC");    
    candlePlotCbcRhsBottom->GetYaxis()->SetTitle("Noise [Vcth]");

   
    candleStackRhs->Add(candlePlotCbcRhsTop);
    candleStackRhs->Add(candlePlotCbcRhsBottom);
    // candleStackRhs->GetXaxis()->SetTitle("CBC");    
    // candleStackRhs->GetYaxis()->SetTitle("Noise [Vcth]");
    candleStackRhs->Draw("VIOLINX("+ViolinDrawString+")");
    lLine->DrawLine(0,0,8,0);
    lCanvas->Write("CandleNoiseRhs_Canvas",TObject::kOverwrite);

    candlePlotCbcRhsTop->Write("candlePlotCbcRhsTop",TObject::kOverwrite);
    candlePlotCbcRhsBottom->Write("candlePlotCbcRhsBottom",TObject::kOverwrite);
    return;
} 
void MakeCandlePlots(const std::vector<std::vector<double>> &pNoisePerFeh,double pLowBin,double pHighBin){
    TCanvas * lCanvas = new TCanvas("CandleNoiseRhs_Canvas","Strip noise candle histograms per CBC - RHS");
    TLine *lLine = new TLine();
    candlePlotCbcRhsTop = new TH2I ("candlePlotCbcRhsTop",TString::Format("Noise distributions per CBC - RHS, Top (resolution: %.3FVcth)",STRIP_NOISE_RESOLUTION),8,0,8, int((pHighBin-pLowBin)/STRIP_NOISE_RESOLUTION),pLowBin,pHighBin);
    candlePlotCbcRhsBottom = new TH2I ("candlePlotCbcRhsBottom",TString::Format("Noise distributions per CBC - RHS, Bottom (resolution: %.3FVcth)",STRIP_NOISE_RESOLUTION),8,0,8, int((pHighBin-pLowBin)/STRIP_NOISE_RESOLUTION),pLowBin,pHighBin);
    
    int cnt{0};
    for (const auto &noiseVal : pNoisePerFeh[0]){
        if (cnt%2 != 0) 
            candlePlotCbcRhsTop->Fill(std::floor(cnt/254.),noiseVal);
        else 
            candlePlotCbcRhsBottom->Fill(std::floor(cnt/254.),noiseVal);
        cnt++;
    }
    THStack *candleStackRhs = new THStack("candleStackRhs","Noise distributions per CBC - RHS Top(blue) and Bottom(red)");
    lCanvas->cd();        
    TString::Format("Noise distributions per CBC - RHS, Bottom (resolution: %.3FVcth)",STRIP_NOISE_RESOLUTION);
    candlePlotCbcRhsTop->SetFillColor(kBlue+1);
    candlePlotCbcRhsTop->SetLineColor(kBlue+1);
    candlePlotCbcRhsTop->SetFillStyle(3004);
    candlePlotCbcRhsBottom->SetFillColor(kRed+2);
    candlePlotCbcRhsBottom->SetLineColor(kRed+2);
    candlePlotCbcRhsBottom->SetFillStyle(3005);
    candlePlotCbcRhsTop->GetXaxis()->SetTitle("CBC");    
    candlePlotCbcRhsTop->GetYaxis()->SetTitle("Noise [Vcth]");
    candlePlotCbcRhsBottom->GetXaxis()->SetTitle("CBC");    
    candlePlotCbcRhsBottom->GetYaxis()->SetTitle("Noise [Vcth]");

   
    candleStackRhs->Add(candlePlotCbcRhsTop);
    candleStackRhs->Add(candlePlotCbcRhsBottom);
    // candleStackRhs->GetXaxis()->SetTitle("CBC");    
    // candleStackRhs->GetYaxis()->SetTitle("Noise [Vcth]");
    candleStackRhs->Draw("VIOLINX("+ViolinDrawString+")");
    lLine->DrawLine(0.,0.,8.,0.);
    lCanvas->Write("CandleNoiseRhs_Canvas",TObject::kOverwrite);

    candlePlotCbcRhsTop->Write("candlePlotCbcRhsTop",TObject::kOverwrite);
    candlePlotCbcRhsBottom->Write("candlePlotCbcRhsBottom",TObject::kOverwrite);
    return;
} 
void MakeStripNoiseCanvi(const std::vector<std::vector<std::pair<double,double>>> &pNoisePerFeh,double pLowY,double pHighY){

    stripNoiseRhsTopE = new TGraphErrors();
    stripNoiseRhsBottomE = new TGraphErrors();
    int cnt{0};
    for (auto noiseVal : pNoisePerFeh[0]){
        if (cnt%2 != 0) {
            auto graphPnt = stripNoiseRhsTopE->GetN();
            stripNoiseRhsTopE->SetPoint(graphPnt,graphPnt,noiseVal.first);
            stripNoiseRhsTopE->SetPointError(graphPnt,0,noiseVal.second);
        } 
        else {
            auto graphPnt = stripNoiseRhsBottomE->GetN();
            stripNoiseRhsBottomE->SetPoint(graphPnt,graphPnt,noiseVal.first);
            stripNoiseRhsBottomE->SetPointError(graphPnt,0,noiseVal.second);
        }
        cnt++;
    }

    TCanvas * lCanvas = new TCanvas("StipNoiseRhs_Canvas","Strip noise - RHS");
    TLine *lLine = new TLine();
    lCanvas->cd();
    stripNoiseRhsTopE->SetLineColor(kBlue+1);
    stripNoiseRhsBottomE->SetLineColor(kRed+2);
    stripNoiseRhsBottomE->GetXaxis()->SetTitle("Strip number [0..1015]");    
    stripNoiseRhsBottomE->GetYaxis()->SetTitle("Noise [Vcth]");
    stripNoiseRhsBottomE->SetTitle("Strip noise - RHS Top(blue) and Bottom(red)");
    stripNoiseRhsBottomE->Draw("APL");
    stripNoiseRhsTopE->Draw("PL same");

    for(int k=0;k<7;k++)
        lLine->DrawLine((127.-0.5)+127.*k,pLowY,(127.-0.5)+127.*k,pHighY);
    stripNoiseRhsTopE->GetYaxis()->SetRangeUser(pLowY,pHighY);
    stripNoiseRhsTopE->GetXaxis()->SetRangeUser(0.,1016.);
    stripNoiseRhsBottomE->GetYaxis()->SetRangeUser(pLowY,pHighY);    
    stripNoiseRhsBottomE->GetXaxis()->SetRangeUser(0.,1016.);    
    lCanvas->Write("StipNoiseRhs_Canvas",TObject::kOverwrite);
    stripNoiseRhsTopE->SetTitle("Strip noise - RHS, Top");
    stripNoiseRhsBottomE->SetTitle("Strip noise - RHS, Bottom");
    stripNoiseRhsTopE->Write("stripNoiseRhsTopE",TObject::kOverwrite);
    stripNoiseRhsBottomE->Write("stripNoiseRhsBottomE",TObject::kOverwrite);
}
void MakeStripNoiseCanvi(const std::vector<std::vector<double>> &pNoisePerFeh,double pLowY,double pHighY){
    stripNoiseRhsTop = new TGraph();
    stripNoiseRhsBottom = new TGraph();
    int cnt{0};
    for (auto noiseVal : pNoisePerFeh[0]){
        if (cnt%2 != 0) 
            stripNoiseRhsTop->SetPoint(stripNoiseRhsTop->GetN(),stripNoiseRhsTop->GetN(),noiseVal);
        else 
            stripNoiseRhsBottom->SetPoint(stripNoiseRhsBottom->GetN(),stripNoiseRhsBottom->GetN(),noiseVal);
        cnt++;
    }

    TCanvas * lCanvas = new TCanvas("StipNoiseRhs_Canvas","Strip noise - RHS");
    TLine *lLine = new TLine();
    lCanvas->cd();
    stripNoiseRhsTop->SetLineColor(kBlue+1);
    stripNoiseRhsBottom->SetLineColor(kRed+2);
    stripNoiseRhsBottom->GetXaxis()->SetTitle("Strip number [0..1015]");    
    stripNoiseRhsBottom->GetYaxis()->SetTitle("Noise [Vcth]");
    stripNoiseRhsTop->GetXaxis()->SetTitle("Strip number [0..1015]");    
    stripNoiseRhsTop->GetYaxis()->SetTitle("Noise [Vcth]");
    stripNoiseRhsBottom->SetTitle("Strip noise - RHS Top(blue) and Bottom(red)");
    stripNoiseRhsBottom->Draw("APL");
    stripNoiseRhsTop->Draw("PL same");

    for(int k=0;k<7;k++)
        lLine->DrawLine((127.-0.5)+127.*k,pLowY,(127.-0.5)+127.*k,pHighY);
    stripNoiseRhsBottom->GetYaxis()->SetRangeUser(pLowY,pHighY);
    stripNoiseRhsBottom->GetXaxis()->SetRangeUser(0.,1016.);
    stripNoiseRhsTop->GetYaxis()->SetRangeUser(pLowY,pHighY);
    stripNoiseRhsTop->GetXaxis()->SetRangeUser(0.,1016.);

    lCanvas->Write("StipNoiseRhs_Canvas",TObject::kOverwrite);
    stripNoiseRhsTop->SetTitle("Strip noise - RHS, Top");
    stripNoiseRhsBottom->SetTitle("Strip noise - RHS, Bottom");
    stripNoiseRhsTop->Write("stripNoiseRhsTop",TObject::kOverwrite);
    stripNoiseRhsBottom->Write("stripNoiseRhsBottom",TObject::kOverwrite);
}

void MakeComparisonCandlePlots(double pLowY,double pHighY){
    TCanvas * lCanvas = new TCanvas("CandleNoiseRhs_Canvas","Strip noise candle histograms per CBC - RHS");
    TLine *lLine = new TLine();
    THStack *candleStackRhs = new THStack("candleStackRhs","Noise distributions per CBC - RHS Top(blue) and Bottom(red)");
    THStack *candleStackRhsTop = new THStack("candleStackRhsTop","Noise distributions per CBC - RHS Top");
    THStack *candleStackRhsBottom = new THStack("candleStackRhsBottom","Noise distributions per CBC - RHS Bottom");
    lCanvas->cd();
 
    candleStackRhs->Add(candlePlotCbcRhsTop_small);
    candleStackRhs->Add(candlePlotCbcRhsTop_large);
    candleStackRhs->Add(candlePlotCbcRhsBottom_small);
    candleStackRhs->Add(candlePlotCbcRhsBottom_large);

    candleStackRhsTop->Add(candlePlotCbcRhsTop_small);
    candleStackRhsTop->Add(candlePlotCbcRhsTop_large);
    candleStackRhsBottom->Add(candlePlotCbcRhsBottom_small);
    candleStackRhsBottom->Add(candlePlotCbcRhsBottom_large);

    candlePlotCbcRhsTop_small->SetFillColor(kBlue);
    candlePlotCbcRhsTop_small->SetLineColor(kBlue);
    candlePlotCbcRhsTop_small->SetFillStyle(3004);
    candlePlotCbcRhsTop_large->SetFillColor(kBlue+2);
    candlePlotCbcRhsTop_large->SetLineColor(kBlue+2);
    candlePlotCbcRhsTop_large->SetFillStyle(3005);
    candlePlotCbcRhsTop_small->GetXaxis()->SetTitle("CBC");    
    candlePlotCbcRhsTop_small->GetYaxis()->SetTitle("Noise [Vcth]");
    candlePlotCbcRhsTop_large->GetXaxis()->SetTitle("CBC");    
    candlePlotCbcRhsTop_large->GetYaxis()->SetTitle("Noise [Vcth]");


    candlePlotCbcRhsBottom_small->SetFillColor(kRed+1);
    candlePlotCbcRhsBottom_small->SetLineColor(kRed+1);
    candlePlotCbcRhsBottom_small->SetFillStyle(3004);
    candlePlotCbcRhsBottom_large->SetFillColor(kRed+2);
    candlePlotCbcRhsBottom_large->SetLineColor(kRed+2);
    candlePlotCbcRhsBottom_large->SetFillStyle(3005);
    candlePlotCbcRhsBottom_small->GetXaxis()->SetTitle("CBC");    
    candlePlotCbcRhsBottom_small->GetYaxis()->SetTitle("Noise [Vcth]");
    candlePlotCbcRhsBottom_large->GetXaxis()->SetTitle("CBC");    
    candlePlotCbcRhsBottom_large->GetYaxis()->SetTitle("Noise [Vcth]");

    candleStackRhs->Draw("VIOLINX("+ViolinDrawString+")");
    candleStackRhs->GetYaxis()->SetTitle("Noise [Vcth]");
    
    auto legend = new TLegend( .125, .75, 0.45, 0.875 );
    legend->SetNColumns(2);
    legend->AddEntry(candlePlotCbcRhsTop_small, "Rhs Top small", "lf");
    legend->AddEntry(candlePlotCbcRhsTop_large, "Rhs Top large", "lf");
    legend->AddEntry(candlePlotCbcRhsBottom_small, "Rhs Bottom small", "lf");
    legend->AddEntry(candlePlotCbcRhsBottom_large, "Rhs Bottom large", "lf");
    legend->Draw();
    lLine->DrawLine(0.,0.,8.,0.);
    // for(int k=0;k<7;k++)
    //     lLine->DrawLine(k+1,pLowY,k+1,pHighY);
    TGaxis *axis = new TGaxis(8,pLowY,8,pHighY,pLowY*VCTH_LVL,pHighY*VCTH_LVL,510,"+L");
    axis->SetName("secondaryAxis");
    axis->SetTitle("Noise [e-]");
    axis->SetLabelSize(0.025);
    axis->SetTextFont(42);
    axis->SetLabelFont(42);
    axis->SetLabelOffset(0.0175);
    axis->Draw();
    lCanvas->SetGridx(1);
    lCanvas->Write("CandleNoiseRhs_smallAndLarge_Canvas",TObject::kOverwrite);

    candlePlotCbcRhsTop_small->Write("candlePlotCbcRhsTop_small",TObject::kOverwrite);
    candlePlotCbcRhsTop_large->Write("candlePlotCbcRhsTop_large",TObject::kOverwrite);
    candlePlotCbcRhsBottom_small->Write("candlePlotCbcRhsBottom_small",TObject::kOverwrite);
    candlePlotCbcRhsBottom_large->Write("candlePlotCbcRhsBottom_large",TObject::kOverwrite);
    candleStackRhsTop->Write("candleStackRhsTop",TObject::kOverwrite);
    candleStackRhsBottom->Write("candleStackRhsBottom",TObject::kOverwrite);
    return;
} 

#endif